/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function showLoading(){
	$('.load-bg').css('display', 'block');
}

function hideLoading(){
	$('.load-bg').css('display', 'none');
}

$(document).ready(function () {
    
   // $('#tab_pagination').DataTable();
   
   $('.sub-nav li').click(function(e) {
    $('.sub-nav li.active').removeClass('active');
    var $this = $(this);
    if (!$this.hasClass('active')) {
        $this.addClass('active');
    }
    //e.preventDefault();
});


});

function logOut() {
    $('.load-bg').css('display', 'block');
    $.ajax({
        type: "POST",
        url: $('#site_url').val() + "user/logout",
        data: {
            logOut: true
        },
        success: function (response) {
            $('.load-bg').css('display', 'none');
            if (response) {
                window.location.replace($('#site_url').val() + "user/login");
            }
        }
    });
    return false;
}
