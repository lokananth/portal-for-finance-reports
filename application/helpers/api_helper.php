<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of api_helper
 *
 * @author Shanmuganathan Velusamy
 */

defined('BASEPATH') OR exit('No direct script access allowed');


function apiGet($apiUrl, $isAuth = true) {
    $headers = array(
        'Authorization: Basic OGFrMzdnSlVZcTJoUno6MGRpdWQ3NjVqZzk0YnNpODRqZmdqMHczamZoNzgyMmo=',
        'Host: 192.168.2.102'
    );
	if (!$isAuth){
		$headers = array();
	}

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $apiUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_USERAGENT, 'geoPlugin PHP Class v1.0');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	if ($isAuth){
    	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	}
    $response = curl_exec($ch);
    if ($response === FALSE) {
        $response = htmlspecialchars(curl_errno($ch));
    }
    curl_close($ch);
    return $response;
}

function apiPost($apiUrl, $data) {

// set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $apiUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen(json_encode($data)),
        'Authorization: Basic OGFrMzdnSlVZcTJoUno6MGRpdWQ3NjVqZzk0YnNpODRqZmdqMHczamZoNzgyMmo=',
        'Host: 192.168.2.102'
    ));
// execute the request
    $output = curl_exec($ch);
    return $output;
}
