<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of new_helper
 *
 * @author Shanmuganathan Velusamy
 */
defined('BASEPATH') OR exit('No direct script access allowed');

function change_date_format($dateString) {
    $newDateString = date_format(date_create_from_format('d-m-Y', $dateString), 'Y-m-d');
    return $newDateString;
}
