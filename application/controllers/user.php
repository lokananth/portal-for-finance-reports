<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 *
 * @author Shanmuganathan Velusamy
 */
//include_once('base_controller.php');

class user extends CI_Controller {

    public function __construct() {
        parent::__construct();
 	date_default_timezone_set('UTC');
        $this->load->helper(array('url', 'form', 'api', 'portal'));
        if (!isset($_SESSION)) {
            session_start();
        }
        $this->load->library('session');
    }

    public function index() {
        //  
        $this->login();
    }

    /**
     * Login
     */
    public function login() {
        $data['title'] = "Sign up";
        $data['main_content'] = 'user/login';
        $this->load->view('login_template', $data);
    }
    
    /**
     * Login Form Submission
     */
    public function login_form_submission() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $data = array(
                "UserName" => $this->input->post('userName'),
                "Password" => $this->input->post('password')
            );
            $response = json_decode(apiPost(config_item('finance_reports_endpoint') . "FRPLogin", $data), TRUE);
            if ($response['errcode'] == "0") {
                // Save Login in session
                $this->session->set_userdata('login', $response['errmsg']);
            }
            echo $response['errcode'];
        }
    }
    
    /**
     * LogOut
     */
    public function logout() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST'):
            $this->session->unset_userdata('login');
            echo json_encode(true);
        endif;
    }

}
