<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 *
 * @author Shanmuganathan Velusamy
 */
include_once('base_controller.php');

class redemption_report extends base_controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        //  
        $this->summary_report();
    }

    /**
     * Summary Report
     */
    public function summary_report() {
        $data['title'] = "Summary Report";
        $data['main_content'] = 'redemption_report/summary_report';
        $this->load->view('template', $data);
    }
    
    /**
     * Get Summary Report
     */
    public function get_summary_report() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            
            $params = array(
                "Product_Code" => $this->input->post('brand'),
                "From_Date" => change_date_format($this->input->post('fromDate')),
                "To_Date" => change_date_format($this->input->post('toDate')),
            );

            $data['dataUsageReport'] = json_decode(apiPost(config_item('finance_reports_endpoint') . "FRPDataUsageSummary", $params), TRUE);
            $_SESSION['dataUsageReport'] = $data['dataUsageReport'];

            $data['bundleSubscriptionsReport'] = json_decode(apiPost(config_item('finance_reports_endpoint') . "FRPBundleSubscriptionSummary", $params), TRUE);
            $_SESSION['bundleSubscriptionsReport'] = $data['bundleSubscriptionsReport'];

            $this->load->view('redemption_report/summary_data_report', $data);
        }
    }

    /**
     * Download Summary Report - Total Usage
     */
    public function download_total_usage_report() {
        //load our new PHPExcel library
        $this->load->library('excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Summary Report - Total Usage');

        for ($col = 'A'; $col != 'J'; $col++) {
            $this->excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
        }

        $this->excel->setActiveSheetIndex(0)->mergeCells('A1:C1');
        $this->excel->getActiveSheet()->setCellValue('A1', 'Summary Report - Total Usage');
        $this->excel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("A1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        
        $this->excel->getActiveSheet()->getStyle('A2:C2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $this->excel->getActiveSheet()->getStyle('A2:C2')->getFill()->getStartColor()->setARGB('58C849');
        $this->excel->getActiveSheet()->getStyle("A2:C2")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $this->excel->getActiveSheet()->getStyle("A2:C2")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A2:C2')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->excel->getActiveSheet()->getStyle('A2:C2')->getFont()->getColor()->setRGB('FFFFFF');;

        $this->excel->setActiveSheetIndex(0)
                ->setCellValue('A2', 'Category')
                ->setCellValue('B2', 'Total Usage - Count(mins/data/sms)')
                ->setCellValue('C2', 'Usage Value(Local Currency)');

        $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
        $this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(20);
        

        $i = 3;

        $dataUsageReport = $_SESSION['dataUsageReport'];
        
        if(!isset($dataUsageReport['errcode'])) {
        foreach ($dataUsageReport AS $row) {
            $this->excel->getActiveSheet()->getStyle('A' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('B' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('C' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
       
            // Miscellaneous glyphs, UTF-8
            $this->excel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $i, $row['Category'])
                    ->setCellValue('B' . $i, $row['Total_Usage_Count'])
                    ->setCellValue('C' . $i, $row['Total_Usage_Value']);
            $i++;
        }
        }

        $filename = 'SummaryReport-TotalUsage-'.time().'.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
		ob_end_clean();
		ob_start();
        $objWriter->save('php://output');
    }
    
    
    /**
     * Download Summary Report - Bundle Subscriptions
     */
    public function download_bundle_subscription_report() {
        //load our new PHPExcel library
        $this->load->library('excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Bundle Subscriptions');

        for ($col = 'A'; $col != 'J'; $col++) {
            $this->excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
        }

        $this->excel->setActiveSheetIndex(0)->mergeCells('A1:E1');
        $this->excel->getActiveSheet()->setCellValue('A1', 'Summary Report - Bundle Subscriptions');
        $this->excel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("A1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        
        $this->excel->getActiveSheet()->getStyle('A2:E2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $this->excel->getActiveSheet()->getStyle('A2:E2')->getFill()->getStartColor()->setARGB('58C849');
        $this->excel->getActiveSheet()->getStyle("A2:E2")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $this->excel->getActiveSheet()->getStyle("A2:E2")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A2:E2')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->excel->getActiveSheet()->getStyle('A2:E2')->getFont()->getColor()->setRGB('FFFFFF');;

        $this->excel->setActiveSheetIndex(0)
                ->setCellValue('A2', 'Category')
                ->setCellValue('B2', 'Count Of Subscriptions(By Voucher)')
                ->setCellValue('C2', 'Count Of Subscriptions(By Credit Card)')
                ->setCellValue('D2', 'Subscription Value(By Voucher)')
                ->setCellValue('E2', 'Subscription Value(By Credit Card)');

        $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
        $this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(20);
        

        $i = 3;

        $bundleSubscriptionsReport = $_SESSION['bundleSubscriptionsReport'];
        
        if(!isset($bundleSubscriptionsReport['errcode'])) {
        foreach ($bundleSubscriptionsReport as $row) {
            $this->excel->getActiveSheet()->getStyle('A' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('B' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('C' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('D' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('E' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
       
            // Miscellaneous glyphs, UTF-8
            $this->excel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $i, $row['Description'])
                    ->setCellValue('B' . $i, $row['Sub_Cunt_by_Voucher'])
                    ->setCellValue('C' . $i, $row['Sub_Cunt_by_CC'])
                    ->setCellValue('D' . $i, $row['Sub_Value_by_Voucher'])
                    ->setCellValue('E' . $i, $row['Sub_Value_by_CC']);
            $i++;
        }
        }

        $filename = 'SummaryReport-BundleSubscriptions-'.time().'.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
		ob_end_clean();
		ob_start();
        $objWriter->save('php://output');
    }
    
    /**
     * Detailed Report
     */
    public function detailed_report() {
        $data['title'] = "Detailed Report";
        $data['main_content'] = 'redemption_report/detailed_report';
        $this->load->view('template', $data);
    }
    
     /**
     * Get Detailed  Report
     */
    public function get_detailed_report() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if ($this->input->post('mode') == "2"):
                $fromDate = date('Y-m-d', strtotime($this->input->post('tdate')));
                $toDate = date('Y-m-d', strtotime($this->input->post('tdate')));
            else:
                $fromDate = change_date_format($this->input->post('fromDate'));
                $toDate = change_date_format($this->input->post('toDate'));
            endif;
            
            $params = array(
                "Product" => $this->input->post('brand'),
                "Mode" => $this->input->post('mode'),
                "Start_Date" => $fromDate,
                "End_date" => $toDate,
                "From_Hrs" => "",
                "To_Hrs" => ""
            );
            
            $_SESSION['dtdRptParams'] = $params;

            $data['params'] = $params;
            $data['detailedReport'] = json_decode(apiPost(config_item('finance_reports_endpoint') . "FRPRedemptionReport", $params), TRUE);

            $this->load->view('redemption_report/detailed_data_report', $data);
        }
    }
    
    /**
     * Print Redemption Detailed Report
     */
    public function print_detailed_report() {
        $params = $_SESSION['dtdRptParams'];
        $data['detailedReport'] = json_decode(apiPost(config_item('finance_reports_endpoint') . "FRPRedemptionReport", $params), TRUE);

        $this->load->view('redemption_report/print_detailed_report', $data);
    }
    
    /**
     * Download Redemption Detailed Report
     */
    public function download_detailed_report() {
        //load our new PHPExcel library
        $this->load->library('excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Detailed Report');

        for ($col = 'A'; $col != 'J'; $col++) {
            $this->excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
        }

        $this->excel->setActiveSheetIndex(0)->mergeCells('A1:N1');
        $this->excel->getActiveSheet()->setCellValue('A1', 'Redemption - Detailed Report');
        $this->excel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("A1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        
        $this->excel->getActiveSheet()->getStyle('A2:N2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $this->excel->getActiveSheet()->getStyle('A2:N2')->getFill()->getStartColor()->setARGB('58C849');
        $this->excel->getActiveSheet()->getStyle("A2:N2")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $this->excel->getActiveSheet()->getStyle("A2:N2")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A2:N2')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->excel->getActiveSheet()->getStyle('A2:N2')->getFont()->getColor()->setRGB('FFFFFF');;

        $this->excel->setActiveSheetIndex(0)
                ->setCellValue('A2', 'Calls')
                ->setCellValue('B2', 'SMS')
                ->setCellValue('C2', 'Data')
                ->setCellValue('D2', 'Bonus Usage-Calls')
                ->setCellValue('E2', 'Bonus Usage-SMS')
                ->setCellValue('F2', 'Bonus Usage-Data')
                ->setCellValue('G2', 'Staff Usage-Calls')
                ->setCellValue('H2', 'Staff Usage-SMS')
                ->setCellValue('I2', 'Staff Usage-Data')
                ->setCellValue('J2', 'Roaming outside EU')
                ->setCellValue('K2', 'Sports News')
                ->setCellValue('L2', 'ATT within Country')
                ->setCellValue('M2', 'ATT to EU')
                ->setCellValue('N2', 'ATT outside EU');
        

        $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
        $this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(20);
        

        $i = 3;

         $params = $_SESSION['dtdRptParams'];

         $detailedReport = json_decode(apiPost(config_item('finance_reports_endpoint') . "FRPRedemptionReport", $params), TRUE);
        
        if(!isset($detailedReport['errcode'])) {
        foreach ($detailedReport as $row) {
            $this->excel->getActiveSheet()->getStyle('A' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('B' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('C' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('D' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('E' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('F' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('G' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('H' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('I' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('J' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('K' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('L' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('M' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('N' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
       
            // Miscellaneous glyphs, UTF-8
            $this->excel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $i, $row['Calls'])
                    ->setCellValue('B' . $i, $row['SMS'])
                    ->setCellValue('C' . $i, $row['Data'])
                    ->setCellValue('D' . $i, $row['Bonus_Usage_Calls'])
                    ->setCellValue('E' . $i, $row['Bonus_Usage_SMS'])
                    ->setCellValue('F' . $i, $row['Bonus_Usage_Data'])
                    ->setCellValue('G' . $i, $row['Staff_Usage_Calls'])
                    ->setCellValue('H' . $i, $row['Staff_Usage_SMS'])
                    ->setCellValue('I' . $i, $row['Staff_Usage_Data'])
                    ->setCellValue('J' . $i, $row['Roaming_Outside_EU'])
                    ->setCellValue('K' . $i, $row['Sports_News'])
                    ->setCellValue('L' . $i, $row['ATT_within_Country'])
                    ->setCellValue('M' . $i, $row['ATT_to_EU'])
                    ->setCellValue('N' . $i, $row['ATT_outside_EU']);
            $i++;
        }
        }

        $filename = 'Redemption-DetailedReport-'.time().'.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
		ob_end_clean();
		ob_start();
        $objWriter->save('php://output');
    }

}