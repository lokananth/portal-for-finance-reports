<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 *
 * @author Shanmuganathan Velusamy
 */
include_once('base_controller.php');

class carrier_usage_report extends base_controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        //  
        $this->by_company();
    }

    /**
     * Carrier Usage Report - By Company
     */
    public function by_company() {
        $data['title'] = "Carrier Usage Report - By Company";
        $data['company'] = json_decode(apiPost(config_item('finance_reports_endpoint') . "FRPGetAllCompany", array()), TRUE);
        $data['reportType'] = "1";
        $data['main_content'] = 'carrier_usage_report/carrier_input_form';
        $this->load->view('template', $data);
    }
    
    /**
     * Get Carrier Usage Report - By Company
     */
    public function get_carrier_usage_report_by_company() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $params = array(
                "Type" => "C",
                "Type_value" => $this->input->post('company'),
                "Country" => "",
                "Fromdate" => change_date_format($this->input->post('fromDate')),
                "Todate" => change_date_format($this->input->post('toDate'))
            );

            $data['byCompanyReport'] = json_decode(apiPost(config_item('finance_reports_endpoint') . "FRPCarrierUtilizationReportCallSMS", $params), TRUE);
            $_SESSION['byCompanyReport'] = $data['byCompanyReport'];
            //echo "<pre>"; echo json_encode($params); echo json_encode($data['byCompanyReport']);
            $this->load->view('carrier_usage_report/by_company', $data);
        }
    }
    
    /**
     * Print Carrier Usage Report - By Company
     */
    public function print_carrier_usage_report_by_company() {
        $data['byCompanyReport'] = $_SESSION['byCompanyReport'];
        $this->load->view('carrier_usage_report/print_by_company', $data);
    }

    /**
     * Download Carrier Usage Report - By Company
     */
    public function download_carrier_usage_report_by_company() {
        //load our new PHPExcel library
        $this->load->library('excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Carrier Usage Report');

        for ($col = 'A'; $col != 'J'; $col++) {
            $this->excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
        }

        $this->excel->setActiveSheetIndex(0)->mergeCells('A1:E1');
        $this->excel->getActiveSheet()->setCellValue('A1', 'Carrier Usage Report - By Company');
        $this->excel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("A1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        
        $this->excel->getActiveSheet()->getStyle('A2:E2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $this->excel->getActiveSheet()->getStyle('A2:E2')->getFill()->getStartColor()->setARGB('58C849');
        $this->excel->getActiveSheet()->getStyle("A2:E2")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $this->excel->getActiveSheet()->getStyle("A2:E2")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A2:E2')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->excel->getActiveSheet()->getStyle('A2:E2')->getFont()->getColor()->setRGB('FFFFFF');;

        $this->excel->setActiveSheetIndex(0)
                ->setCellValue('A2', 'Country')
                ->setCellValue('B2', 'Calls')
                ->setCellValue('C2', 'SMS')
                ->setCellValue('D2', 'Data')
                ->setCellValue('E2', 'Total Charge');

        $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
        $this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(20);
        

        $i = 3;

        $byCompanyReport = $_SESSION['byCompanyReport'];
        
        if(!isset($byCompanyReport['errcode'])) {
        foreach ($byCompanyReport AS $row) {
            $this->excel->getActiveSheet()->getStyle('A' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('B' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('C' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('D' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('E' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
       
            // Miscellaneous glyphs, UTF-8
            $this->excel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $i, $row['Country'])
                    ->setCellValue('B' . $i, $row['CALL'])
                    ->setCellValue('C' . $i, $row['SMS'])
                    ->setCellValue('D' . $i, "N/A")
                    ->setCellValue('E' . $i, $row['TotalCharge']);
            $i++;
        }
        }

        $filename = 'CarrierUsageReport-ByCompany-'.time().'.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
		ob_end_clean();
		ob_start();
        $objWriter->save('php://output');
    }
    
    /**
     * Carrier Usage Report - By Carrier
     */
    public function by_carrier() {
        $data['title'] = "Carrier Usage Report - By Carrier";
        $params =  array(
          "Company_ID" => "0",
          "Country" => "UK"
        );
        $data['company'] = json_decode(apiPost(config_item('finance_reports_endpoint') . "FRPCarrierUsageReportByCarrier", $params), TRUE);
        $data['reportType'] = "2";
        $data['main_content'] = 'carrier_usage_report/operator_input_form';
        $this->load->view('template', $data);
    }
    
    /**
     * Get Carrier Usage Report - By Carrier
     */
    public function get_carrier_usage_report_by_carrier() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $params = array(
                "Type" => "O",
                "Type_value" => $this->input->post('company'),
                "Country" => "",
                "Fromdate" => change_date_format($this->input->post('fromDate')),
                "Todate" => change_date_format($this->input->post('toDate'))
            );

            $data['byCarrierReport'] = json_decode(apiPost(config_item('finance_reports_endpoint') . "FRPCarrierUtilizationReportCallSMS", $params), TRUE);
            $_SESSION['byCarrierReport'] = $data['byCarrierReport'];
            $this->load->view('carrier_usage_report/by_carrier', $data);
        }
    }
    
    /**
     * Print Carrier Usage Report - By Carrier
     */
    public function print_carrier_usage_report_by_carrier() {
        $data['byCarrierReport'] = $_SESSION['byCarrierReport'];
        $this->load->view('carrier_usage_report/print_by_carrier', $data);
    }
    
     /**
     * Download Carrier Usage Report - By Carrier
     */
    public function download_carrier_usage_report_by_carrier() {
        //load our new PHPExcel library
        $this->load->library('excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Carrier Usage Report');

        for ($col = 'A'; $col != 'J'; $col++) {
            $this->excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
        }

        $this->excel->setActiveSheetIndex(0)->mergeCells('A1:E1');
        $this->excel->getActiveSheet()->setCellValue('A1', 'Carrier Usage Report - By Carrier');
        $this->excel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("A1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        
        $this->excel->getActiveSheet()->getStyle('A2:E2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $this->excel->getActiveSheet()->getStyle('A2:E2')->getFill()->getStartColor()->setARGB('58C849');
        $this->excel->getActiveSheet()->getStyle("A2:E2")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $this->excel->getActiveSheet()->getStyle("A2:E2")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A2:E2')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->excel->getActiveSheet()->getStyle('A2:E2')->getFont()->getColor()->setRGB('FFFFFF');;

        $this->excel->setActiveSheetIndex(0)
                ->setCellValue('A2', 'Country')
                ->setCellValue('B2', 'Calls')
                ->setCellValue('C2', 'SMS')
                ->setCellValue('D2', 'Data')
                ->setCellValue('E2', 'Total Charge');

        $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
        $this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(20);
        

        $i = 3;

        $byCarrierReport = $_SESSION['byCarrierReport'];
        
        if(!isset($byCarrierReport['errcode'])) {
        foreach ($byCarrierReport AS $row) {
            $this->excel->getActiveSheet()->getStyle('A' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('B' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('C' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('D' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('E' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
       
            // Miscellaneous glyphs, UTF-8
            $this->excel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $i, $row['Country'])
                    ->setCellValue('B' . $i, $row['CALL'])
                    ->setCellValue('C' . $i, $row['SMS'])
                    ->setCellValue('D' . $i, "N/A")
                    ->setCellValue('E' . $i, $row['TotalCharge']);
            $i++;
        }
        }

        $filename = 'CarrierUsageReport-ByCarrier-'.time().'.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
		ob_end_clean();
		ob_start();
        $objWriter->save('php://output');
    }
    
    /**
     * Carrier Usage Report - By Country
     */
    public function by_country() {
        $data['title'] = "Carrier Usage Report - By Country";
        $data['country'] = json_decode(apiPost(config_item('finance_reports_endpoint') . "FRPGetAllCountry", array()), TRUE);
        $data['main_content'] = 'carrier_usage_report/carrier_form_by_country';
        $this->load->view('template', $data);
    }
    
    /**
     * Get Carrier Usage Report - By Carrier
     */
    public function get_carrier_usage_report_by_country() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $params = array(
                "Type" => "",
                "Type_value" => "",
                "Country" => $this->input->post('country'),
                "Fromdate" => change_date_format($this->input->post('fromDate')),
                "Todate" => change_date_format($this->input->post('toDate'))
            );
            
            $data['params'] = $params;

            $data['byCountryReport'] = json_decode(apiPost(config_item('finance_reports_endpoint') . "FRPCarrierUtilizationReportCallSMS", $params), TRUE);
            $_SESSION['byCountryReport'] = $data['byCountryReport'];
           // echo "<pre>"; echo json_encode($params); echo json_encode($data['byCountryReport']);
            $this->load->view('carrier_usage_report/by_country', $data);
        }
    }
    
    /**
     * Print Carrier Usage Report - By Country
     */
    public function print_carrier_usage_report_by_country() {
        $data['byCountryReport'] = $_SESSION['byCountryReport'];
        $this->load->view('carrier_usage_report/print_by_country', $data);
    }
    
    /**
     * Download Carrier Usage Report - By Country
     */
    public function download_carrier_usage_report_by_country() {
        //load our new PHPExcel library
        $this->load->library('excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Carrier Usage Report');

        for ($col = 'A'; $col != 'J'; $col++) {
            $this->excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
        }

        $this->excel->setActiveSheetIndex(0)->mergeCells('A1:E1');
        $this->excel->getActiveSheet()->setCellValue('A1', 'Carrier Usage Report - By Country');
        $this->excel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("A1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        
        $this->excel->getActiveSheet()->getStyle('A2:E2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $this->excel->getActiveSheet()->getStyle('A2:E2')->getFill()->getStartColor()->setARGB('58C849');
        $this->excel->getActiveSheet()->getStyle("A2:E2")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $this->excel->getActiveSheet()->getStyle("A2:E2")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A2:E2')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->excel->getActiveSheet()->getStyle('A2:E2')->getFont()->getColor()->setRGB('FFFFFF');;

        $this->excel->setActiveSheetIndex(0)
                ->setCellValue('A2', 'Carrier')
                ->setCellValue('B2', 'Calls')
                ->setCellValue('C2', 'SMS')
                ->setCellValue('D2', 'Data')
                ->setCellValue('E2', 'Total Charge');

        $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
        $this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(20);
        

        $i = 3;

        $byCountryReport = $_SESSION['byCountryReport'];
        
        if(!isset($byCountryReport['errcode'])) {
        foreach ($byCountryReport AS $row) {
            $this->excel->getActiveSheet()->getStyle('A' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('B' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('C' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('D' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('E' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
       
            // Miscellaneous glyphs, UTF-8
            $this->excel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $i, $row['Operator_name'])
                    ->setCellValue('B' . $i, $row['CALL'])
                    ->setCellValue('C' . $i, $row['SMS'])
                    ->setCellValue('D' . $i, "N/A")
                    ->setCellValue('E' . $i, $row['TotalCharge']);
            $i++;
        }
        }
        
        $filename = 'CarrierUsageReport-ByCountry-'.time().'.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
		ob_end_clean();
		ob_start();
        $objWriter->save('php://output');
    }

}