<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>:: FRP - Mundio Mobiles ::</title>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="generator" content="Bootply" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="shortcut icon" href="<?php echo $this->config->item('get_drupal_base_url'); ?>/sites/all/themes/bootstrap/favicon.ico" type="image/vnd.microsoft.icon" />
        <?php $this->load->view('layouts/header_script') ?>
        <?php $this->load->view('layouts/header') ?>
    </head>
    <body>
        <div class="load-bg" style="display: none;">
          <span class="loader"></span>
        </div>
        <div class="container1 content-top-space col-xs-12">  
            <div class="row">
                <?php $this->load->view('layouts/side_bar') ?>
                <div class="col-sm-10 rightContainer">
                    <?php $this->load->view($main_content) ?>
                </div>
            </div>
        </div>
        <?php $this->load->view('layouts/footer') ?>
    </body>
</html>
