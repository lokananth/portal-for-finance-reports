<div class="col-sm-2 sideBar">

    <!--<div id="myAffix" data-spy="affix" data-offset-top="150" data-offset-bottom="200">
Sidebar -->
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav finance-side-bar">
            <li class="sidebar-brand">
                <a href="#">
                    FRP
                </a>
            </li>
            <li class="mainmenu">
                <a href="#">Redemption Report <span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
                <ul class="sub-nav">
                    <li id="menu1" class="active"><a href="<?php echo site_url('redemption_report/summary_report');?>">Summary Report</a></li>
                    <li id="menu2"><a href="<?php echo site_url('redemption_report/detailed_report');?>">Detailed Report</a></li>
                </ul>
            </li>
            <li class="mainmenu">
                <a href="#">Carrier Usage Report <span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
                <ul class="sub-nav">
                    <li id="menu3"><a href="<?php echo site_url('carrier_usage_report/by_company');?>">By Company</a></li>
                    <li id="menu4"><a href="<?php echo site_url('carrier_usage_report/by_carrier');?>">By Carrier</a></li>
                    <li id="menu5"><a href="<?php echo site_url('carrier_usage_report/by_country');?>">By Country</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <!--</div>
     /#sidebar-wrapper -->
</div>

<script type="text/javascript">
    $(document).ready(function() {
   var url = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
   $('.finance-side-bar li ').removeClass('active');
   if(url === "detailed_report") {
       $("#menu2").addClass('active');
   }
   else if(url === "summary_report") {
       $("#menu1").addClass('active');
   }
   else if(url === "by_company") {
       $("#menu3").addClass('active');
   }
   else if(url === "by_carrier") {
       $("#menu4").addClass('active');
   }
   else if(url === "by_country") {
       $("#menu5").addClass('active');
   }
   else {
       $("#menu1").addClass('active');
   }
  });
</script>
