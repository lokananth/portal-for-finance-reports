<div id="navigation-menu" class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class=" container1">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" rel="home" href="#"><img src="<?php echo base_url("assets/img/logo-mundio.png"); ?>" style="max-width:186px;" border="0" alt="" /></a>
        </div>
        <input type="hidden" id="site_url" value="<?php echo base_url(); ?>" />
        <div class="collapse navbar-collapse">

            <ul class="nav navbar-nav mobileDisplay">
                <!--<li><a href="#">Users</a></li>-->
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Redemption Report <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo site_url('redemption_report/summary_report');?>">Summary Report</a></li>
                        <li><a href="<?php echo site_url('redemption_report/detailed_report');?>">Detailed Report</a></li>

                        <!--<li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                        <li class="divider"></li>
                        <li><a href="#">One more separated link</a></li>-->
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Carrier Usage Report <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo site_url('carrier_usage_report/by_company');?>">By Company</a></li>
                        <li><a href="<?php echo site_url('carrier_usage_report/by_carrier');?>">By Carrier</a></li>
                        <li><a href="<?php echo site_url('carrier_usage_report/by_country');?>">By Country</a></li>
                        <li class="divider"></li>
                        <!--<li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                        <li class="divider"></li>
                        <li><a href="#">One more separated link</a></li>-->
                    </ul>
                </li>
            </ul>

            <div class="col-xs-12 col-sm-10 col-md-7 pull-right Mpd-0">
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right"> 
                        <li class="dropdown">
                            <a class="dropdown-toggle" role="button" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-user"></span> Admin <span class="caret"></span></a>
                            <ul id="g-account-menu" class="dropdown-menu" role="menu">
                                <li><a href="#" onclick="return logOut();">Log Out</a></li>
                            </ul>
                        </li>
                    </ul>
                    <div class="navbar-text pull-right">Welcome, Mundio Finance	</div>

                </div>
            </div>
        </div>
    </div>
</div>