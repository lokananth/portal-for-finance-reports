<section class="siptrunking-banner">
    <div class="container">
        <div class="banner-content">
            <a href="#" class="page-name" onclick="javascript:return false;" style="cursor: auto;"><?php echo $title; ?></a>
            <img src="<?php echo isset($banner_img) ? base_url("assets/images/$banner_img") : base_url("assets/images/SIPTrunkbannericon.png"); ?>" alt="user image" class="loginIcon" />
        </div>
    </div>
</section>