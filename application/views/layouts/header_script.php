<link href="<?php echo base_url("assets/css/bootstrap.min.css"); ?>" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url("assets/css/styles.css"); ?>" />
<link rel="stylesheet" href="<?php echo base_url("assets/css/custome.css"); ?>" />
<link rel="stylesheet" href="<?php echo base_url("assets/css/bootstrap-datepicker.css"); ?>"  />
<link rel="apple-touch-icon" href="<?php echo base_url("assets/bootstrap/img/apple-touch-icon.png"); ?>">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url("assets/bootstrap/img/apple-touch-icon-72x72.png"); ?>">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url("assets/bootstrap/img//bootstrap/img/apple-touch-icon-114x114.png"); ?>">


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/bootstrap.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/main.js"); ?>"></script>
<script src="http://cdn.jsdelivr.net/jquery.validation/1.14.0/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/bootbox.min.js"); ?>"></script>
<script type='text/javascript' src="<?php echo base_url("assets/js/datatable-source.js"); ?>"></script>
<script type='text/javascript' src="https://cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/bootstrap-datepicker.js"); ?>"></script>
