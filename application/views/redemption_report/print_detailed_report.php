<link href="<?php echo base_url("assets/css/bootstrap.min.css"); ?>" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url("assets/css/styles.css"); ?>" />
<link rel="stylesheet" href="<?php echo base_url("assets/css/custome.css"); ?>" />
<div class="col-sm-12">
    <div align="center">
        <h4><?php echo "Redemption Detailed Report"; ?></h4>
    </div>
    <div class="row">
    <div class="pull-right" style="margin-bottom: 5px; margin-right:15px;">
        <button class="btn btn-primary" onclick="javascript:window.print();">Print</button>
    </div>
    </div>
    <br />
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-responsive table-hover" id="tab_pagination">
            <thead>
                <tr>
                    <th>Calls</th>
                    <th>SMS</th>
                    <th>Data</th>
                    <th>Bonus Usage-Calls</th>
                    <th>Bonus Usage-SMS</th>
                    <th>Bonus Usage-Data</th>
                    <th>Staff Usage-Calls</th>
                    <th>Staff Usage-SMS</th>
                    <th>Staff Usage-Data</th>
                    <th>Roaming outside EU</th>
                    <th>Sports News</th>
                    <th>ATT within Country</th>
                    <th>ATT to EU</th>
                    <th>ATT outside EU</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (!isset($detailedReport['errcode'])):
                    foreach ($detailedReport as $row):
                        ?>
                        <tr>
                            <td><?php echo $row['Calls']; ?></td>
                            <td><?php echo $row['SMS']; ?></td>
                            <td><?php echo $row['Data']; ?></td>
                            <td><?php echo $row['Bonus_Usage_Calls']; ?></td>
                            <td><?php echo $row['Bonus_Usage_SMS']; ?></td>
                            <td><?php echo $row['Bonus_Usage_Data']; ?></td>
                            <td><?php echo $row['Staff_Usage_Calls']; ?></td>
                            <td><?php echo $row['Staff_Usage_SMS']; ?></td>
                            <td><?php echo $row['Staff_Usage_Data']; ?></td>
                            <td><?php echo $row['Roaming_Outside_EU']; ?></td>
                            <td><?php echo $row['Sports_News']; ?></td>
                            <td><?php echo $row['ATT_within_Country']; ?></td>
                            <td><?php echo $row['ATT_to_EU']; ?></td>
                            <td><?php echo $row['ATT_outside_EU']; ?></td>
                        </tr>
    <?php endforeach;
endif; ?>
            </tbody>
        </table>
    </div>
</div>
