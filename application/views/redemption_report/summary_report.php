<div class="col-xs-12">          
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title">
                <h4>Summary Report</h4>
            </div>
        </div>
        <div class="panel-body">
            <form class="form form-horizontal" id="frmSummaryReport" name="frmSummaryReport" action="<?php echo site_url('/redemption_report/login_form_submission'); ?>">
                <div class="form-group col-sm-3 col-xs-12">
                    <label for="selectBrand">Brand</label>
                    <div class="controls">
                        <select id="selectBrand" name="selectBrand" class="form-control">
                            <option value="0">Select Brand</option>
                            <?php $i=1; foreach (config_item('brand') as $brand): ?>
                              <option value="<?php echo $i; ?>"><?php echo $brand; ?></option>
                            <?php $i++; endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="form-group col-sm-3 col-xs-12">
                    <label>Start Date</label>
                    <div class="input-group date col-sm-12 from_date">
                        <input readonly="readonly" type="text" class="form-control dates" id="fromDate"  placeholder="dd-mm-yyyy" name="fromDate">
                        <div class="input-group-addon"> <span class="glyphicon glyphicon-th"></span> </div>
                    </div>
                </div>
                <div class="form-group  col-sm-3 col-xs-12">
                    <label>End Date</label>
                    <div class="input-group date col-sm-12 to_date">
                        <input readonly="readonly" type="text" class="form-control dates" placeholder="dd-mm-yyyy" id="toDate" name="toDate">
                        <div class="input-group-addon"> <span class="glyphicon glyphicon-th"></span> </div>
                    </div>
                </div>
                               
                
                <div class="form-group col-sm-3 col-xs-12">
                    <label>&nbsp;</label>
                    <div class="controls">
                        <button class="btn btn-primary" type="submit"><span class="glyphicon glyphicon-search"></span>
                            Search
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <!--/panel content-->
    </div>
    <div id="summaryDataReport"></div>
    
    

    
    
</div>

<script type="text/javascript">
    $(document).ready(function () {
//    $('.date').datepicker({
//        autoclose: true,
//        format: 'dd-mm-yyyy',
//        //endDate : new Date(currentDate),
//        orientation: "top left"
//
//    });
//    
//    $('#to-date').datepicker({
//        autoclose: true,
//        format: 'dd-mm-yyyy',
//        //endDate : new Date(currentDate),
//        orientation: "top left"
//    });
    
    var FromEndDate = new Date();
    $(".from_date").datepicker({
      format: 'dd-mm-yyyy',
      autoclose: true,
      orientation: "top left",
      endDate: FromEndDate,
    }).on('changeDate', function (selected) {
    var startDate = new Date(selected.date.valueOf());
    $('.to_date').datepicker('setStartDate', startDate);
    $("#fromDate-error").css('display', 'none');
    }).on('clearDate', function (selected) {
    $('.to_date').datepicker('setStartDate', null);
    });

    $(".to_date").datepicker({
      format: 'dd-mm-yyyy',
      autoclose: true,
      orientation: "top left",
      endDate: FromEndDate,
    }).on('changeDate', function (selected) {
    var endDate = new Date(selected.date.valueOf());
    $('.from_date').datepicker('setEndDate', endDate);
    $("#toDate-error").css('display', 'none');
    }).on('clearDate', function (selected) {
    $('.from_date').datepicker('setEndDate', null);
    });
    
    $('#tab_pagination').DataTable();
    
    $("#frmSummaryReport").validate({
        rules: {
            selectBrand:{
                required: true,
                min: 1
            },
            fromDate: "required",
            toDate: "required"
	},
        messages: {
            selectBrand: {
                required: "Please select brand",
                min: "Please select brand"
            },
            fromDate: "Please select start date",
            toDate: "Please select end date",
	},
        submitHandler: function (form) {
            showLoading();
            $.ajax({
            type: "POST",
            url: $('#site_url').val() + "redemption_report/get_summary_report",
            data: {
                brand: $("#selectBrand option:selected").text(),
                fromDate: $("#fromDate").val(),
                toDate: $("#toDate").val(),
            },
            success: function (response) {
                hideLoading();
                //alert(response);
                $('#summaryDataReport').html(response);
            }
        });
      }
    });
    
    $('#myTabs a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
  });

    
    });
</script>    