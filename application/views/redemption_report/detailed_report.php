<div class="col-xs-12">          
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title">
                <h4>Detailed Report</h4>
            </div>
        </div>
        <div class="panel-body">
            <form class="form form-horizontal" id="frmDetailedReport" name="frmDetailedReport" action="<?php echo site_url('/redemption_report/login_form_submission'); ?>">
                <div class="form-group col-sm-6 col-md-3 col-xs-12">
                    <label for="selectBrand">Brand</label>
                    <div class="controls">
                        <select id="selectBrand" name="selectBrand" class="form-control">
                            <option value="0">Select Brand</option>
                            <?php $i=1; foreach (config_item('brand') as $brand): ?>
                              <option value="<?php echo $i; ?>"><?php echo $brand; ?></option>
                            <?php $i++; endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="form-group col-sm-6 col-md-4  col-lg-2 col-xs-12 pd-0">
                    <div style="min-height:60px; padding-top:30px;">
                        <label class="col-md-6 pd-0">
                            <input type="radio" id="radioMonthly" name="mode" checked="checked"  value="1" /> Monthly
                        </label> 
                        <label class="col-md-6 pd-0" >
                            <input type="radio" id="radioDaily" name="mode" value="2" /> Daily
                        </label> 
<!--                        <label class="col-md-4 pd-0" >
                            <input type="radio" id="radioHourly" name="mode" value="3" /> Hourly
                        </label> -->
                    </div>
                </div>
	<div class="col-sm-12 col-md-4 pull-left pd-0">
                <div id="monthlyDatePicker" class="col-sm-12 Tm-t-20 pd-0 col-xs-12">
                <div class="form-group col-sm-6  col-md-6   col-lg-6 col-xs-12 Mm-b-20">
                    <label>Start Date</label>
                    <div class="input-group date col-sm-12 from_date">
                        <input readonly="readonly" type="text" class="form-control dates" id="fromDate"  placeholder="dd-mm-yyyy" name="fromDate">
                        <div class="input-group-addon"> <span class="glyphicon glyphicon-th"></span> </div>
                    </div>
                </div>
                <div class="form-group  col-sm-6  col-md-6   col-lg-6 col-xs-12 Mm-b-20">
                    <label>End Date</label>
                    <div class="input-group date col-sm-12 to_date">
                        <input readonly="readonly" type="text" class="form-control dates" placeholder="dd-mm-yyyy" id="toDate" name="toDate">
                        <div class="input-group-addon"> <span class="glyphicon glyphicon-th"></span> </div>
                    </div>
                </div>
                </div>
                
                <div id="dailyDatePicker" class="form-group col-sm-6  col-md-6 col-xs-12" style="display: none;">
                    <label>Date</label>
                    <div class="input-group date col-sm-12 tDate">
                        <input readonly="readonly" type="text" class="form-control dates" id="tDate"  placeholder="dd-mm-yyyy" name="tDate">
                        <div class="input-group-addon"> <span class="glyphicon glyphicon-th"></span> </div>
                    </div>
                </div>
                
                <div class="form-group col-sm-6 col-md-6 col-xs-12" id="hourlyDatePicker" style="display: none;">
                    <label for="selectHour">Select Hour</label>
                    <div class="controls">
                        <select id="selectHour" name="selectHour" class="form-control">
                            <?php $i=1; foreach (config_item('hours_range') as $hour): ?>
                              <option value="<?php echo $i; ?>"><?php echo $hour; ?></option>
                            <?php $i++; endforeach; ?>
                        </select>
                    </div>
                </div>
                               
                </div>
                <div class="form-group col-sm-6 col-md-2 col-xs-12 Mm-t-20">
                    <label class="displayM ">&nbsp;</label>
                    <div class="controls">
                        <button class="btn btn-primary" type="submit"><span class="glyphicon glyphicon-search"></span>
                            Search
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <!--/panel content-->
    </div>
    <div id="detailedDataReport"></div>
    
    

    
    
</div>

<script type="text/javascript">
    $(document).ready(function () {
    var FromEndDate = new Date();    
    $('.tDate').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy',
        //endDate : new Date(currentDate),
        orientation: "top left",
        endDate: FromEndDate,

    });
//    
//    $('#to-date').datepicker({
//        autoclose: true,
//        format: 'dd-mm-yyyy',
//        //endDate : new Date(currentDate),
//        orientation: "top left"
//    });
    
    
    $(".from_date").datepicker({
      format: 'dd-mm-yyyy',
      autoclose: true,
      orientation: "top left",
      endDate: FromEndDate,
    }).on('changeDate', function (selected) {
    var startDate = new Date(selected.date.valueOf());
    $('.to_date').datepicker('setStartDate', startDate);
    $("#fromDate-error").css('display', 'none');
    }).on('clearDate', function (selected) {
    $('.to_date').datepicker('setStartDate', null);
    });

    $(".to_date").datepicker({
      format: 'dd-mm-yyyy',
      autoclose: true,
      orientation: "top left",
      endDate: FromEndDate,
    }).on('changeDate', function (selected) {
    var endDate = new Date(selected.date.valueOf());
    $('.from_date').datepicker('setEndDate', endDate);
    $("#toDate-error").css('display', 'none');
    }).on('clearDate', function (selected) {
    $('.from_date').datepicker('setEndDate', null);
    });
    
    $('#tab_pagination').DataTable();
    
    $("#frmDetailedReport").validate({
        rules: {
            selectBrand:{
                required: true,
                min: 1
            },
            fromDate: "required",
            toDate: "required",
            tDate: "required"
	},
        messages: {
            selectBrand: {
                required: "Please select brand",
                min: "Please select brand"
            },
            fromDate: "Please select start date",
            toDate: "Please select end date",
            tDate: "Please select date",
	},
        submitHandler: function (form) {
            showLoading();
            $.ajax({
            type: "POST",
            url: $('#site_url').val() + "redemption_report/get_detailed_report",
            data: {
                brand: $("#selectBrand option:selected").text(),
                mode: $("input[type='radio']:checked").val(),
                fromDate: $("#fromDate").val(),
                toDate: $("#toDate").val(),
                tdate: $("#tDate").val()
            },
            success: function (response) {
                hideLoading();
                //alert(response);
                $('#detailedDataReport').html(response);
            }
        });
      }
    });
    
    $('#myTabs a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
  });
  
   $("input[type='radio']").click(function(){
      $("#fromDate").val("");
      $("#toDate").val("");
      $("#tDate").val("");
      if($(this).val() == "1") {
          $("#monthlyDatePicker").css('display', 'block');
          $("#hourlyDatePicker").css('display', 'none');
          $("#dailyDatePicker").css('display', 'none');
      } 
      else if($(this).val() == "2") {
          $("#monthlyDatePicker").css('display', 'none');
          $("#hourlyDatePicker").css('display', 'none');
          $("#dailyDatePicker").css('display', 'block');
      }
      else if($(this).val() == "3") {
          $("#monthlyDatePicker").css('display', 'none');
          $("#hourlyDatePicker").css('display', 'block');
          $("#dailyDatePicker").css('display', 'block');
      }
   });

    
    });
</script>    
