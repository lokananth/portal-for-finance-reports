<?php //echo "<pre>"; print_r($params); print_r($detailedReport); echo "</pre>"; ?>
<div class="col-sm-12 pd-0">
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-responsive table-hover" id="tab_paginationa">
            <thead>
                <tr>
                    <th>Calls</th>
                    <th>SMS</th>
                    <th>Data</th>
                    <th>Bonus Usage-Calls</th>
                    <th>Bonus Usage-SMS</th>
                    <th>Bonus Usage-Data</th>
                    <th>Staff Usage-Calls</th>
                    <th>Staff Usage-SMS</th>
                    <th>Staff Usage-Data</th>
                    <th>Roaming outside EU</th>
                    <th>Sports News</th>
                    <th>ATT within Country</th>
                    <th>ATT to EU</th>
                    <th>ATT outside EU</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (!isset($detailedReport['errcode'])):
                    foreach ($detailedReport as $row):
                        ?>
                        <tr>
                            <td><?php echo $row['Calls']; ?></td>
                            <td><?php echo $row['SMS']; ?></td>
                            <td><?php echo $row['Data']; ?></td>
                            <td><?php echo $row['Bonus_Usage_Calls']; ?></td>
                            <td><?php echo $row['Bonus_Usage_SMS']; ?></td>
                            <td><?php echo $row['Bonus_Usage_Data']; ?></td>
                            <td><?php echo $row['Staff_Usage_Calls']; ?></td>
                            <td><?php echo $row['Staff_Usage_SMS']; ?></td>
                            <td><?php echo $row['Staff_Usage_Data']; ?></td>
                            <td><?php echo $row['Roaming_Outside_EU']; ?></td>
                            <td><?php echo $row['Sports_News']; ?></td>
                            <td><?php echo $row['ATT_within_Country']; ?></td>
                            <td><?php echo $row['ATT_to_EU']; ?></td>
                            <td><?php echo $row['ATT_outside_EU']; ?></td>
                        </tr>
    <?php endforeach;
endif; ?>
            </tbody>
        </table>
    </div>
    
     <div class="pull-right">
         <a href="<?php echo base_url('redemption_report/print_detailed_report'); ?>" onclick="window.open(this.href, 'windowName', 'width=1500, height=700, left=24, top=24, scrollbars, resizable'); return false;"><button class="btn btn-primary" >Print</button></a>
        <button class="btn btn-primary" id="detailedReportDownlod">Download</button>
    </div>  

</div>

<script type="text/javascript">
    $(document).ready(function() { 
         $('#tab_pagination').DataTable();
         
         $("#detailedReportDownlod").click(function() {
             window.location.replace($('#site_url').val() + "redemption_report/download_detailed_report");
         });
         
        
    });
</script>