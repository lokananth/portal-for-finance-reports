
<div class="col-sm-12 pd-0">
            <ul class="nav nav-tabs" id="myTabs" role="tablist">
                <li class="active"><a href="#home" role="tab" data-toggle="tab">Total Usage - Talk Charge</a></li>
                <li><a href="#profile" role="tab" data-toggle="tab">Bundle Subscriptions</a></li>          
            </ul><!--/.nav-tabs.content-tabs -->
            <div class="tab-content">
                <div class="tab-pane fade in active" id="home">
                    <div class="table-responsive">
        <table class="table table-striped table-bordered table-responsive table-hover" id="tab_pagination">
            <thead>
                <tr>
                    <th>Category</th>
                    <th>Total Usage - Count(mins/data/sms)</th>
                    <th>Usage Value(Local Currency)</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                if(!isset($dataUsageReport['errcode'])):
                foreach($dataUsageReport as $row): ?>
                <tr>
                    <td><?php echo $row['Category']; ?></td>
                    <td><?php echo $row['Total_Usage_Count']; ?></td>
                    <td><?php echo $row['Total_Usage_Value']; ?></td>
                </tr>
                <?php endforeach; endif; ?>
            </tbody>
        </table>
    </div>
    <div class="pull-right">
        <!--<button class="btn btn-primary" >Print</button>-->
        <button class="btn btn-primary" id="dataUsageDownlod">Download</button>
    </div>    
                </div><!--/.tab-pane -->
                <div class="tab-pane fade" id="profile">
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-responsive table-hover" id="tab_pagination">
            <thead>
                <tr>
                    <th>Category</th>
                    <th>Count Of Subscriptions(By Voucher) </th>
                    <th>Count Of Subscriptions(By Credit Card)</th>
                    <th>Subscription Value(By Voucher)</th>
                    <th>Subscription Value(By Credit Card)</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                if(!isset($bundleSubscriptionsReport['errcode'])):
                foreach($bundleSubscriptionsReport as $row): ?>
                <tr>
                    <td><?php echo $row['Description']; ?></td>
                    <td><?php echo $row['Sub_Cunt_by_Voucher']; ?></td>
                    <td><?php echo $row['Sub_Cunt_by_CC']; ?></td>
                    <td><?php echo $row['Sub_Value_by_Voucher']; ?></td>
                    <td><?php echo $row['Sub_Value_by_CC']; ?></td>
                </tr>
                <?php endforeach;endif;?>
               
            </tbody>
        </table>
    </div>
                    <div class="pull-right">
                        <button class="btn btn-primary" id="bundleSubDownlod">Download</button>
                    </div>
                </div><!--/.tab-pane -->
            </div><!--/.tab-content -->                                                     
    </div>

<script type="text/javascript">
    $(document).ready(function() {
         $('#myTabs a').click(function (e) {
         e.preventDefault()
         $(this).tab('show')
         });
         
         $('#tab_pagination').DataTable();
         
         $("#dataUsageDownlod").click(function() {
             window.location.replace($('#site_url').val() + "redemption_report/download_total_usage_report");
         });
         
         $("#bundleSubDownlod").click(function() {
             window.location.replace($('#site_url').val() + "redemption_report/download_bundle_subscription_report");
         });
    });
</script>