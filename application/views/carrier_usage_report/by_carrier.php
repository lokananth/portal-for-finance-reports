
<div class="col-sm-12 pd-0">
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-responsive table-hover" id="tab_pagination">
            <thead>
                <tr>
                    <th>Country</th>
                    <th>Calls</th>
                    <th>SMS</th>
                    <th>Data</th>
                    <th>Total Charge</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (!isset($byCarrierReport['errcode'])):
                    foreach ($byCarrierReport as $row):
                        ?>
                        <tr>
                            <td><?php echo $row['Country']; ?></td>
                            <td><?php echo $row['CALL']; ?></td>
                            <td><?php echo $row['SMS']; ?></td>
                            <td>N/A</td>
                            <td><?php echo $row['TotalCharge']; ?></td>
                        </tr>
    <?php endforeach;
endif; ?>
            </tbody>
        </table>
    </div>
    <br />
     <div class="pull-right">
        <a href="<?php echo base_url('carrier_usage_report/print_carrier_usage_report_by_carrier'); ?>" onclick="window.open(this.href, 'windowName', 'width=1000, height=700, left=24, top=24, scrollbars, resizable'); return false;"><button class="btn btn-primary" >Print</button></a>
        <button class="btn btn-primary" id="carrierUsageDownlod">Download</button>
    </div>  

</div>

<script type="text/javascript">
    $(document).ready(function() { 
         $('#tab_pagination').DataTable();
         
         $("#carrierUsageDownlod").click(function() {
             window.location.replace($('#site_url').val() + "carrier_usage_report/download_carrier_usage_report_by_carrier");
         });
         
    });
</script>