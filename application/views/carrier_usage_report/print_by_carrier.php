<link href="<?php echo base_url("assets/css/bootstrap.min.css"); ?>" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url("assets/css/styles.css"); ?>" />
<link rel="stylesheet" href="<?php echo base_url("assets/css/custome.css"); ?>" />
<div class="col-sm-12">
    <div align="center">
        <h4><?php echo "Carrier Usage Report - By Carrier"; ?></h4>
    </div>
    <div class="row">
        <div class="pull-right" style="margin-bottom: 5px; margin-right: 15px;">
            <button class="btn btn-primary" onclick="javascript:window.print();">Print</button>
        </div>
    </div>
    <br />
  
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-responsive table-hover" id="tab_pagination">
            <thead>
                <tr>
                    <th>Country</th>
                    <th>Calls</th>
                    <th>SMS</th>
                    <th>Data</th>
                    <th>Total Charge</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (!isset($byCarrierReport['errcode'])):
                    foreach ($byCarrierReport as $row):
                        ?>
                        <tr>
                            <td><?php echo $row['Country']; ?></td>
                            <td><?php echo $row['CALL']; ?></td>
                            <td><?php echo $row['SMS']; ?></td>
                            <td>N/A</td>
                            <td><?php echo $row['TotalCharge']; ?></td>
                        </tr>
                    <?php
                    endforeach;
                endif;
                ?>
            </tbody>
        </table>
    </div>
</div>