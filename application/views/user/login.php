<div class="container">    
    <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 dropIcon">      <!--style="margin-top:8%;"-->               
        <div class="panel panel-primary pull-left" >
            <div class="panel-heading bgblue pull-left col-xs-12 text-center">
                <div class="panel-title">
                    <a class="navbar-brand  pull-left col-xs-12 text-center pd-0" rel="home" style="font-size:14px;">
                        <img src="<?php echo base_url("assets/img/logo-mundio.png"); ?>" class="logo pull-left" border="0" alt="">
                        <div class="lcrlogintitle pull-right">FRP Systems</div>
                    </a></div>                        
            </div>     

            <div class="panel-body pull-left col-xs-12" >
                <h3 class="mr-0 lcrlogin">Login to your account</h3>
                <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>

                <form id="loginform" name="loginform" class="form-horizontal" role="form" method="post" style="padding-top:30px" >
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input id="txtUsername" type="text" class="form-control" name="txtUsername" value="" placeholder="username" autocomplete="off">                                    
                    </div>

                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input id="txtPassword" type="password" class="form-control" name="txtPassword" placeholder="password" autocomplete="off">
                    </div>            
                    <div class="form-group">
                        <!-- Button -->

                        <div class="col-sm-5 controls pd-0">
                            <input type="submit" name="login" id="login" value="Login" class="btn btn-primary pull-left" />
                        </div>
                        <div class="col-sm-5 controls pd-0"> 
                            <div style="color:red; margin-left:15px; margin-top:5px; font-size: 13px;" id="login_msg"></div>
                        </div>
                    </div>
                    
                </form>     
            </div>                     
        </div>  
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {

        $("#loginform").validate({
        rules: {
            txtUsername:{
                required: true,
                email: true
            },
            txtPassword: "required"
	},
        messages: {
            txtUsername: {
                required: "Please enter username",
                email: "Please enter valid email address as username"
            },
            txtPassword: "Please enter the valid password",
	},
        submitHandler: function (form) {
           // showLoading();
           $('#login_msg').html('<img src="' + $('#site_url').val() + 'assets/images/loader.gif" />');
            $.ajax({
            type: "POST",
            url: $('#site_url').val() + "user/login_form_submission",
            data: {
                userName: $("#txtUsername").val(),
                password: $("#txtPassword").val()
            },
            success: function (response) {
             //   hideLoading();
                if (response == 0 || response == "0") {
                    window.location.replace($('#site_url').val()+"redemption_report/index");
                }
                else {
                    $("#login_msg").html("Invalid Login");
                }
            }
        });
      }
    });
       
       $('input').keypress(function() {
		$("#login_msg").html('');
	});
       
       $('#txtUsername').keydown(function(e){
        if (e.which === 32) {
            e.preventDefault();      
        }
        }).blur(function() {
            $(this).val(function(i,oldVal){
                return oldVal.replace(/\s/g,'');
            });         
       });
       
       $("input").mousedown(function(e) //Right click
       {
        if(e.which == 3) //1: left, 2: middle, 3: right
        {
            $("#login_msg").html('');
        }
       });
    });
</script>